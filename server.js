'use strict'
var https = require('https');

var express = require('express');
var server = express();

var server_config = require('./server_config')

var app = require('./app/app.js');

app.start(express, server);

server.listen(server_config.port, function () {
  console.log('server listening on', server_config.port);
});
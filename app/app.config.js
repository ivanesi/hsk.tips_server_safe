'use strict'

module.exports = {
    
    files_dir: '/home/webadmin/server/apps/hsk.tips/app/files/',

    cnm_eng_media_dir: {
		root: '/home/webadmin/server/apps/hsk.tips/app/files/media/',
		audio: '/home/webadmin/server/apps/hsk.tips/app/files/media/audio/',
		video: '/home/webadmin/server/apps/hsk.tips/app/files/media/video/',
		images: '/home/webadmin/server/apps/hsk.tips/app/files/media/images/'
	},

	logs_dir: '/home/webadmin/server/apps/hsk.tips/app/var/logs/app'
	
}

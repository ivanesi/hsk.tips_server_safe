var logger = require('./logger');

module.exports = function (req, res, next) {
    var _req = {
        ip: req.ip,
        ips:req.ips,
        //headers: req.headers, 
        url: req.url,
        originalUrl: req.originalUrl,
        //path: req.path,
        query: req.query,
        params: req.params,
        user: req.session.user_email,
        gauth_date: req.session.gauth_date
        // ..and more
        // see http://expressjs.com/ru/api.html#req

    }

    logger.log(_req)

    next();
};
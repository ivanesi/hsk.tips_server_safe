var logger = require('./logger');

module.exports = function (err, req, res, next) {
   res.send(500, err);   
   var error = {url:req.originalUrl, err:err, user: req.session.user_email}
   logger.error(error)
   next();
}
const app_config = require('../app.config');

module.exports = require('tracer').dailyfile({root:app_config.logs_dir, maxLogFiles: 15});
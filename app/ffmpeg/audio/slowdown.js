'use strict'

const process = require('child_process');
const helpers = require('../../helpers/files_and_dirs');

module.exports = function(inF, outF) {
    console.log('slowdown')
    var extention = helpers.get_file_extention(inF);
    
    if (!outF) {
        outF = helpers.get_filename_without_extention(inF)+'-slow.'+extention;
    }
    
    return new Promise ( (resolve, reject) => {
        var slowdown = process.spawn('ffmpeg', ['-i', inF, '-filter:a', 'atempo=0.65', '-vn', '-y', outF] )
        slowdown.stderr.on('close', (code) => resolve(outF) )
        slowdown.stderr.on('error', (err) => reject(err) );
    })
}
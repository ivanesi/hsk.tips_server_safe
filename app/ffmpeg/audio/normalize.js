'use strict'

const logger = require('../../logger/logger');
const fs = require('fs');
const process = require('child_process');

module.exports = function(f, newFileName) {
   return _detectMaxLevel(f, newFileName)
        .then( res => _normalize(f, ( (res.level*-1) -5), res.newFileName) )
        .catch(err => logger.error(err) )
}

function _detectMaxLevel(f, newFileName) {
    
    return new Promise ( (resolve, reject) => {
        var calcMaxLevel = process.spawn('ffmpeg', ['-i', f, '-af', 'volumedetect', '-f', 'null', '/NULL'] )

        var totalData = '';
            
        calcMaxLevel.stderr.on('data', (data) => {
            if (data) totalData += data;
        })

        calcMaxLevel.stderr.on('close', (code) => {
            
            var indx = totalData.indexOf('max_volume:');
            var level = 1*totalData.substring(indx+12, totalData.indexOf('dB', indx)-1)
            resolve({level, newFileName});
        })

        calcMaxLevel.stderr.on('error', (err) => reject(err) )
    
    })
}

function _normalize(f, db, newFileName) {
    console.log(f)
    var extention = f.split('.').pop();
    if (!newFileName) newFileName = f+'_tmp.'+extention;

    return new Promise ( (resolve, reject) => {
        var normalizeProcess = process.spawn('ffmpeg', ['-i', f, '-af', 'volume='+db+'dB', '-y', newFileName] )
        normalizeProcess.stderr.on('close', (code) => {
            if (newFileName == (f+'_tmp.'+extention) ) {        
                fs.rename(newFileName, f, (err) => {
                    if (err) reject(err)
                    resolve(f);
                })
            } else resolve(newFileName) 
        })
        normalizeProcess.stderr.on('error', (err) => reject(err) );
    });                    
};
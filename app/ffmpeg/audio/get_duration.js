'use strict'

const process = require('child_process');

module.exports = function(f) {
    return new Promise ( (resolve, reject) => {

        var calcDurationProcess = process.spawn('ffprobe', [f]);
        var totalData = '';
        
        calcDurationProcess.stderr.on('data', (data) => {
            if (data) totalData += data;
        })

        calcDurationProcess.stderr.on('close', (code) => {
            var indxDur = totalData.indexOf('Duration:');
            var x = totalData.substring(indxDur+13,indxDur+ 21)
            var msec = ''
            for (var i = 0; i<9; i++) {
                if (x.charAt(i) != ':' && x.charAt(i) != '.') {
                    msec+=x.charAt(i)
                }
            }
            resolve(msec * 10)
        });

        calcDurationProcess.stderr.on('error', (err) => reject(err) )
    
    });
}
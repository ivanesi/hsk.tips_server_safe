'use strict'
const fs = require('fs');

function create_dir_if_need_sync(dir) {
    if(!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
    }
}

function save_file_to_dir(file, dir) {
    
    if (file.new_name) { file.name = file.new_name };

    return new Promise ( (resolve, reject) => {
                  
        var file_path = dir + file.name;

        file.mv(file_path, (err) => {
            if (err) reject(err)
            return resolve(file_path);
        })
    })
}

function delete_file(file) {
    return new Promise ( (resolve, reject) => {
        fs.stat(file, (err, stats) => {
            if (!err) {
                fs.unlink(file, (err) => {
                    if (err) reject(err);
                    return resolve(file);
                })
            } else return resolve(null);
        })
        
    })
}

function get_file_extention(filename) {
    if (filename.indexOf('.')) {
        return filename.split('.').pop()
    } else return ''
}

function get_filename_without_extention(filename) {
    if (filename.indexOf('.')) {
        return filename.substr(0, filename.lastIndexOf('.'))
    } else return filename
}

function get_filename_from_path(path) {
    return path.substr(path.lastIndexOf('/')+1)
}

function get_path_from_full_path(path) {
    return path.substr(0, path.lastIndexOf('/')+1)
}

module.exports = {
    create_dir_if_need_sync,
    save_file_to_dir,
    delete_file,
    get_file_extention,
    get_filename_without_extention,
    get_filename_from_path,
    get_path_from_full_path
}
'use strict'

const base64url = require('base64url');

var oAuth2_config = require('./oauth2_config');

var google = require('../google');

var OAuth2 = google.auth.OAuth2;

var get_oauth2_client = function() {
    return new OAuth2 (oAuth2_config.client_id,
                       oAuth2_config.client_secret,
                       oAuth2_config.redirect_url )
}

var oauth2_url = null;
var generate_oauth2_url = function(oauth2_client) {
    if (!oauth2_url) {
        console.log('generate_oauth2_url')
        oauth2_url = oauth2_client.generateAuthUrl({
        access_type: oAuth2_config.access_type,
        scope: oAuth2_config.scopes,
        })
    }
    return oauth2_url
};

var check_google_code = function(req) {
    console.log('check_google_code')
    return new Promise( function(resolve, reject) {
        if (req.query.error) {
            reject(req.query.error)
        }
        if (!req.query.code) { 
            reject('err: no google code')
        } else resolve (req.query.code)        
    })
}

var get_access_tokens = function(oauth2_client, code) {
    console.log('get_access_token')
    return new Promise( function(resolve, reject) {
        oauth2_client.getToken(code, function (err, tokens) {
            if (err) {
                reject(err);
            } else {
                resolve(tokens);
            }
            
        });
    })
};


var save_tokens_in_session = function(req, tokens) {
    console.log('save_tokens_in_session')
    return new Promise( function(resolve, reject) {
        if (req.session) {
            req.session.gauth_date = Date.now();            
            req.session.tokens = tokens;
            req.session.save();
            resolve();
        } else {
            reject(err)
        }
    })
}

var get_user_data = function(oauth2_client) {
    console.log('getting user data')
    return new Promise( function(resolve, reject) {
        google.oauth2("v2").userinfo.get({
            auth: oauth2_client
        }, function(err, userinfo) {
            if (err) {
                console.log(err)
                reject(err)
            } else {
                console.log(userinfo.email)
                resolve(userinfo)            
            }
        });
    })
}

var refresh_access_token_if_need = function(req, oauth2_client) {
    console.log('refresh_access_token_if_need')
    var expiry_in = req.session.tokens.expiry_date - Date.now();
    return new Promise( (resolve, reject) => {
        if (expiry_in < 120000) { // less than in 2 min
            console.log('> need, refreshing')
            console.log(expiry_in/1000/60, 'mins')
            oauth2_client.refreshAccessToken(function(err, tokens) {
                if (err) { 
                    reject(err)
                    console.log(err)
                    console.log('> session:')
                    console.log(req.session)
                    console.log('> oauth2_client:')
                    console.log(oauth2_client)
                    console.log('>tokens:');
                    console.log(tokens)
                    console.log()
                    console.log('app/google/oauth2/oauth2.js')
                }
                req.session.tokens = tokens;
                req.session.save();
                resolve(tokens);
            })
        } else {
            console.log('> no need, we have', expiry_in/1000/60, 'mins')
            resolve(req.session.tokens);
        }
    })
}


module.exports = {
    generate_oauth2_url: generate_oauth2_url,

    get_oauth2_client: get_oauth2_client,
    check_google_code: check_google_code,
    get_access_tokens: get_access_tokens,
    save_tokens_in_session: save_tokens_in_session,

    refresh_access_token_if_need:refresh_access_token_if_need,
    
    get_user_data: get_user_data
}
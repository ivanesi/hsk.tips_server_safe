
'use strict'
const logger = require('../../logger/logger');
const google = require('../google');
const oauth2 = require('../oauth2/oauth2');
const fs = require('fs')
var readline = require('readline');

var insert_video = function(req, options) {

    var opt = {
            part: 'id,snippet,status', 
            notifySubscribers: true,
            resource: {
                snippet: {
                    title: options.title,
                    description: options.description,
                    tags: options.tags
                },
                status: {
                    privacyStatus: "public",
                }               
            },
            media: {
                    body: fs.createReadStream(options.filename)
            }     
        }

    console.log(options.filename)
    return new Promise( function(resolve, reject) {
        var oauth2_client = oauth2.get_oauth2_client();
        oauth2_client.setCredentials(req.session.tokens);
        oauth2.refresh_access_token_if_need(req, oauth2_client)
        .then( () => {
            var upload_process = google.youtube({
                version: 'v3',
                auth: oauth2_client
            }).videos.insert(opt, (err, data) => {
                if (err) reject(err)
                //if (err) console.log(err)
                if (data) {
                    resolve(data)    
                }
            }) 
            
            var fileSize = fs.statSync(options.filename).size
            console.log((fileSize / 1000000).toFixed(2)+'Mb')

            var id = setInterval(function () {
            var uploadedBytes = upload_process.req.connection._bytesDispatched;
            var uploadedMBytes = uploadedBytes / 1000000;
            var progress = uploadedBytes > fileSize
                  ? 100 : (uploadedBytes / fileSize) * 100;
            readline.cursorTo(process.stdout, 0);
            // process.stdout.clearLine();
            // process.stdout.cursorTo(0);
            process.stdout.write(uploadedMBytes.toFixed(2) + ' MBs uploaded. ' +
            progress.toFixed(2) + '% completed.');
            if (progress === 100) {
                  process.stdout.write('\nDone uploading, waiting for response...\n');
                  clearInterval(id);
            }
            }, 500);      


        }).catch(err => logger.error(err) )
    })

}

module.exports = {
    insert_video
}
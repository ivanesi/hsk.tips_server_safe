'use strict'
const playlists = require('./playlists');
const videos = require('./videos');

module.exports = {
    playlists: playlists,
    videos: videos 
}

'use strict'

var google = require('../google');
var oauth2 = require('../oauth2/oauth2');

var logger = require('../../logger/logger');


var get_playlists = function(req) {
    var opt = {
            part: 'snippet',
            mine: 'true',
            maxResults: '25',
        };
    if (req.query.pageToken) {
        opt.pageToken = req.query.pageToken;
    };

    return new Promise( function(resolve, reject) {

        var oauth2_client = oauth2.get_oauth2_client();
        oauth2_client.setCredentials(req.session.tokens);
        oauth2.refresh_access_token_if_need(req, oauth2_client)
        .then( () => {            
            google.youtube({
                version: 'v3',
                auth: oauth2_client
            }).playlists.list(opt, (err, playlists) => {
                if (err) reject(err)
                resolve(playlists)                             
            });
        }).catch(err => logger.error(err) )
    });
    
}

var get_all_playlists = function(req) {
    console.log('get_all_playlists')
    var all_playlists = [];
    return new Promise( (resolve, reject) => {
            var response;

            _get_pl(req, resolve);       

            function _get_pl(req, resolve) {
                get_playlists(req).then(resp => {
                    all_playlists = all_playlists.concat(resp.items)
                    response = resp;                
                    if (response.nextPageToken) { 
                        req.query.pageToken = response.nextPageToken;
                        _get_pl(req, resolve)
                    } else {
                        return resolve(all_playlists)
                    }
                });        
            }
        })
}

var get_playlistItems = function(req) {
    console.log('get_playlistItems')

    var opt = {
            playlistId: req.query.playlistId,
            part: 'snippet',
            mine: 'true',
            maxResults: '50',
        };

    if (req.query.pageToken) {
        opt.pageToken = req.query.pageToken;
    };

    return new Promise( function(resolve, reject) {

        var oauth2_client = oauth2.get_oauth2_client();
        oauth2_client.setCredentials(req.session.tokens);
        oauth2.refresh_access_token_if_need(req, oauth2_client)
        .then( () => {            
            google.youtube({
                version: 'v3',
                auth: oauth2_client
            }).playlistItems.list(opt, (err, playlistItems) => {
                if (err) reject(err)                
                resolve(playlistItems)                             
            });
        }).catch(err => logger.error(err) )
    });
}

var get_all_playlistItems = function(req) {
    console.log('get_all_playlistItems')
    var all_playlistItmes = [];
    return new Promise( (resolve, reject) => {
            var response;

            _get_plItems(req, resolve);       

            function _get_plItems(req, resolve) {
                get_playlistItems(req).then(resp => {
                    all_playlistItmes = all_playlistItmes.concat(resp.items)
                    response = resp;                
                    if (response.nextPageToken) { 
                        req.query.pageToken = response.nextPageToken;
                        _get_plItems(req, resolve)
                    } else {
                        console.log('>>>>finish')                        
                        return resolve(all_playlistItmes)
                    }
                });        
            }
        })
}

var insert_playlist = function(req) {

    var opt = {
            part: 'snippet, status', 
            resource: {
                snippet: {
                    title: req.body.title,
                    description: req.body.description,
                    tags: req.body.tags
                },
                status: {
                    privacyStatus: "public",
                }
            }         
        }

    return new Promise( function(resolve, reject) {
        var oauth2_client = oauth2.get_oauth2_client();
        oauth2_client.setCredentials(req.session.tokens);
        oauth2.refresh_access_token_if_need(req, oauth2_client)
        .then( () => {
            google.youtube({
                version: 'v3',
                auth: oauth2_client
            }).playlists.insert(opt, (err, response) => {
                if (err) reject(err)
                resolve(response)    
            })
        }).catch(err => logger.error(err) )
    })
}

var insert_playlistItems = function(req) {

    var opt = {
        part: 'snippet', 
        resource: {
            snippet: {
                playlistId: req.body.playlistId,
                resourceId: {
                    kind: 'youtube#video',
                    videoId: req.body.videoId
                }
            }       
        }
    }

    return new Promise( function(resolve, reject) {
        var oauth2_client = oauth2.get_oauth2_client();
        oauth2_client.setCredentials(req.session.tokens);
        oauth2.refresh_access_token_if_need(req, oauth2_client)
        .then( () =>{
            google.youtube({
                version: 'v3',
                auth: oauth2_client
            }).playlistItems.insert(opt, (err, response) => {
                if (err) reject(err)
                resolve(response)         
            })  
        }).catch(err => logger.error(err) )
    })      
}

module.exports = {
    get_playlists,
    get_all_playlists,
    insert_playlist,
    insert_playlistItems,
    get_playlistItems,
    get_all_playlistItems
}
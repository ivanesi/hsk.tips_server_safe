'use strict'

function skip (skip) {
    if ( skip > 0 ) {
        return parseInt(skip);
    } else {
        return 0
    }
}

function limit (limit) {
    if ( limit > 0 ) {
        return parseInt(limit);
    } else {
        return 0
    }
}

function notEmptyArr (arr) {
    if (arr.length <= 0 ) {
        arr = [ {null:null} ]
    }
    return arr
}

function escapingRegExp(word) {
    return word.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
}

module.exports = {
    skip: skip,
    limit: limit,
    notEmptyArr: notEmptyArr,
    escapingRegExp:escapingRegExp
}

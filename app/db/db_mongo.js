'use strict'

module.exports.MongoClient = require('mongodb').MongoClient;
module.exports.assert = require('assert');
module.exports.ObjectID = require('mongodb').ObjectID;
'use strict'

const mongo = require('../../db_mongo');
const db_config = require('../db_config');
const db_methods = require('../../_db_methods/db_methods');

var logger = require('../../../logger/logger');

var params_for_get_hsk_words = require('../helpers/get_hsk_words_params');

var db_user = db_config.users.guest.login,
    db_pwd = db_config.users.guest.pwd,
    db_host = db_config.host,
    db_port = db_config.port,
    db_name = db_config.name,
    db_colls = db_config.collections;

    
const db_url = 'mongodb://' +db_user + ":" +db_pwd + '@' +db_host + ':' +db_port +'/'+ db_name;

console.log('TODO: db_logger');

var get_hsk_wordlists_basic_info = function() {
    
    var req_params = {
      db_url: db_url,
      find_criteria: {},
      projection: { _id:0 },
      db_coll: db_colls.wordlistsInfoHSK
    }

    return new Promise( (resolve) => {
      db_methods.find(req_params)
      .then( result => resolve(result) )
      .catch(err => logger.error(err) )
    });
}

var get_hsk_words = function(req) {
      var opt = {
        reqQuery: req.query,
        admin: false
      }
      var req_params = params_for_get_hsk_words(opt);
      req_params.db_url = db_url;
      req_params.db_coll = db_colls.wordsHSK;
      
      return new Promise( (resolve) => {
        db_methods.count(req_params)
        .then( (count) => {
          if (count > 0) {
            return db_methods.find(req_params)
            .then(result => resolve({ words:result, total:count } ))
            .catch(err => logger.error(err))
          } else resolve({total:count});         
        })
        .catch(err => logger.error(err))
      });
}

module.exports = {
  get_hsk_wordlists_basic_info: get_hsk_wordlists_basic_info,
  get_hsk_words : get_hsk_words
}
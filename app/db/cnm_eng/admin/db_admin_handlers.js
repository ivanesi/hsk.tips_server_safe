'use strict'

const ObjectID = require('../../db_mongo').ObjectID;
const db_config = require('../db_config');
const db_methods = require('../../_db_methods/db_methods');

const helpers = require('../../../helpers/files_and_dirs');

var logger = require('../../../logger/logger');

var params_for_get_hsk_words = require('../helpers/get_hsk_words_params');

var db_user = db_config.users.admin.login,
    db_pwd = db_config.users.admin.pwd,
    db_host = db_config.host,
    db_port = db_config.port,
    db_name = db_config.name,
    db_colls = db_config.collections;

    
const db_url = 'mongodb://' +db_user + ":" +db_pwd + '@' +db_host + ':' +db_port +'/'+ db_name;

console.log('TODO: db_logger');

var get_hsk_wordlists_basic_info = function() {    
    var req_params = {
      db_url: db_url,
      find_criteria: {},
      projection: {},
      db_coll: db_colls.wordlistsInfoHSK
    }

    return new Promise( (resolve) => {
      db_methods.find(req_params)
      .then( result => resolve(result) )
      .catch(err => logger.error(err) )
    });
}

var get_hsk_words = function(req) {
     var opt = {
        reqQuery: req.query,
        admin: false
      }
      var req_params = params_for_get_hsk_words(opt);
      req_params.db_url = db_url;
      req_params.db_coll = db_colls.wordsHSK;
      
      return new Promise( (resolve) => {
        db_methods.count(req_params)
        .then( (count) => {
          if (count > 0) {
            return db_methods.find(req_params)
            .then(result => resolve({ words:result, total:count } ))
            .catch(err => logger.error(err))
          } else resolve({total:count});         
        })
        .catch(err => logger.error(err))
      });
}

var get_one_word_by_id = function(_id) {
    console.log('get one word by _id')
    
    var opt = {
      db_url: db_url,
      find_criteria: {_id: new ObjectID(_id) },
      projection: {},
      db_coll: db_colls.wordsHSK
    }
    return db_methods.find_one(opt)
}

var update_one_word = function(find_criteria, update) {
   
   if (find_criteria._id) find_criteria._id = new ObjectID(find_criteria._id)
   if (update._id) delete update._id
   
    var options = {
        db_url: db_url,
        db_coll: db_colls.wordsHSK,
        update: update,
        find_criteria: find_criteria
    }
    return db_methods.update_one(options)
}

var save_media_to_db = function(req, files) {
  console.log('db_admin_handlers.save_media_to_db')
  
  var what = req.query.what;

  var find_criteria = {
        _id: req.query.w_id
  }

  files.norm.f = helpers.get_filename_from_path(files.norm.f);
  if (files.slow.f) files.slow.f = helpers.get_filename_from_path(files.slow.f);

  var update = {};

  if (req.query.operation === 'update_word') {
      if (what === 'hw') {
        update.$set = {};
        update.$set["mp3." + req.query.hw_index + ".norm.f"] = files.norm.f; 
        update.$set["mp3." + req.query.hw_index + ".norm.dur"] = Number(files.norm.dur); 
        update.$set["mp3." + req.query.hw_index + ".slow.f"] = files.slow.f; 
        update.$set["mp3." + req.query.hw_index + ".slow.dur"] = Number(files.slow.dur); 
      }
      if (what === 'ex') {
        find_criteria["ex.eid"] = Number(req.query.ex_id);
        update.$set = {};
        update.$set["ex.$.mp3.norm.f"] = files.norm.f; 
        update.$set["ex.$.mp3.norm.dur"] = Number(files.norm.dur); 
        update.$set["ex.$.mp3.slow.f"] = files.slow.f; 
        update.$set["ex.$.mp3.slow.dur"] = Number(files.slow.dur); 
        
      }
      if (what === 'ex_tr') { 
        find_criteria["ex.eid"] = Number(req.query.ex_id);
        update.$set = {}
        update.$set["ex.$.tr.mp3.f"] = files.norm.f;
        update.$set["ex.$.tr.mp3.dur"] = Number(files.norm.dur)
      } 

      if (what === 'new_ex') {
        update.$push = {}
        update.$push.ex = {
                        "src" : "",
                        "hw_p1" : "",
                        "eid" : Number(req.query.ex_id),
                        "redZ_s" : "",
                        "txt_s" : "",
                        "p1" : "",
                        "p2" : "",
                        "tr" : {
                                "txt" : "",
                                "mp3" : {
                                        "f" : null,
                                        "dur" : null,
                                        "tts" : ''
                                }
                        },
                        "mp3" : {
                                "norm" : {
                                        "f" : files.norm.f,
                                        "dur" : Number(files.norm.dur)
                                },
                                "slow" : {
                                        "f" : files.slow.f,
                                        "dur" : Number(files.slow.dur)
                                }
                        },
                        "ytid" : null
                }
        
      }
      if (what === 'new_ex_tr')  {
        update.$push = {}
        update.$push.ex = {
                        "src" : "",
                        "hw_p1" : "",
                        "eid" : Number(req.query.ex_id),
                        "redZ_s" : "",
                        "txt_s" : "",
                        "p1" : "",
                        "p2" : "",
                        "tr" : {
                                "txt" : "",
                                "mp3" : {
                                        "f" : files.norm.f,
                                        "dur" : Number(files.norm.dur),
                                        "tts" : ''
                                }
                        },
                        "mp3" : {
                                "norm" : {
                                        "f" : null,
                                        "dur" : null
                                },
                                "slow" : {
                                        "f" : null,
                                        "dur" : null
                                }
                        },
                        "ytid" : null
                }
      }

      return update_one_word(find_criteria, update)

  } else console.log('add new word? look at: db/cnm_eng/admin/')

  
}

var save_ytid_to_db = function(w, ex, video_data) {
  console.log(video_data.id)
  if (video_data.id) {
    console.log('save_ytid_to_db')  
    
    var find_criteria = { _id: w._id }
    find_criteria["ex.eid"] = Number(ex.eid);
    
    var update = {};
    update.$set = {};
    update.$set["ex.$.ytid"] = video_data.id;;
    
    return update_one_word(find_criteria, update)

  } else return video_data

}

var get_ex_toDel = function() {
    console.log('get_ex_toDel');

    var opt = {
      db_url: db_url,
      db_coll: db_colls.wordsHSK
    }

    opt.find_criteria = {}
    opt.find_criteria["ex.marks"] = "toDel"
    
    opt.projection = { hsk_id:1, s:1, hsk:1, p1:1, _id:0 }
    opt.projection["ex.$.eid"] = 1
    
    return db_methods.find(opt)

}

module.exports = {
  get_hsk_wordlists_basic_info,
  get_hsk_words,
  get_one_word_by_id,
  update_one_word,
  save_media_to_db,
  save_ytid_to_db,
  get_ex_toDel
}
'use strict'

var _helpers = require('./_helpers');
var validate = require('../../validate');

// params for db_api_guest_handlers.get_hsk_words
module.exports = function(opt) {
  var reqQuery = opt.reqQuery;
  // will return it
  var db_req_params = {
      find_criteria: {},
      projection: {},
      skip: 0,
      limit: 25
  }

  var levels = _helpers.isArr(reqQuery.l),
      wordlists = _helpers.isArr(reqQuery.wl),
      word = reqQuery.w;

  // skip 
  // if skip < 0 mongo fall!
  db_req_params.skip = validate.skip(reqQuery.skip)
  
  // projection
  if (!opt.admin) {
    db_req_params.projection = {
      _id:1,
      s:1,
      hsk_id:1,
      hsk:1,
      p1:1,
      means:1, 
    };

    db_req_params.projection['ex.txt_s'] = 1;
    db_req_params.projection['ex.p1'] = 1;
    db_req_params.projection['ex.ytid'] = 1;
    db_req_params.projection['ex.tr.txt'] = 1;
  } 
  
  // find_criteria
  var find_criteria = {};
      
      find_criteria.$and = [];

  // levels
  if (levels.length != 0) {
    let indx = find_criteria.$and.push( { $or: [] } ) - 1;
    levels.forEach( (lev) => {
      find_criteria.$and[indx].$or.push( { hsk : lev } )
    })
  } else {
    find_criteria.levels = [1,2,3,4,5,6]
  };

  // wordlists
  if (wordlists.length != 0) {
    let indx = find_criteria.$and.push( { $or: [] } ) - 1;
    wordlists.forEach( (wlist) => {
      find_criteria.$and[indx].$or.push( { wordlists : wlist } )
    })
    find_criteria.$and[indx].$or = _helpers.uniqArr(find_criteria.$and[indx].$or);      
  }

  // word
  if (word) {
    word = validate.escapingRegExp(word);
    let indx = find_criteria.$and.push( { $or: [] } ) - 1;
    find_criteria.$and[indx].$or.push( { s :  { $regex: new RegExp(word), $options: 'i' } } )
    find_criteria.$and[indx].$or.push( { p3 :  { $regex: new RegExp(word), $options: 'i' } } )
  }
  
  //validate $and and $or = should be not empty array (and $nor)
  find_criteria.$and.forEach((and, i) => {
    find_criteria.$and[i].$or = validate.notEmptyArr(and.$or);
  })
  find_criteria.$and = validate.notEmptyArr(find_criteria.$and);

  db_req_params.find_criteria = find_criteria;


  return db_req_params

}

'use strict'

function isArr(reqObj) {
    var arr = [];
    if (typeof(reqObj) == 'string') {  
        arr.push(reqObj);
    } else {
        arr = reqObj;
    }
    
    if (!reqObj) arr = [];
    
    return arr
}

  
function uniqArr(arr) {
    var o = {}, i, l = arr.length, r = [];
    for(i = 0; i < l; i++) o[arr[i].wordlists] = arr[i].wordlists;
    for(i in o) r.push( {wordlists:o[i]} );
    return r;
}

module.exports.isArr = isArr;
module.exports.uniqArr = uniqArr;

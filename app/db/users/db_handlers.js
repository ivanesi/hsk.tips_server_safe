'use strict'

const mongo = require('../db_mongo');
const db_config = require('../db_global_config');
const db_methods = require('../_db_methods/db_methods');

const db_user = db_config.dbs.users.users.admin.name,
      db_pwd = db_config.dbs.users.users.admin.pwd,
      db_host = db_config.dbs.users.host,
      db_port = db_config.dbs.users.port,
      db_name = db_config.dbs.users.name,
      db_colls = db_config.dbs.users.collections;

var logger = require('../../logger/logger');
    
const db_url = 'mongodb://' +db_user + ":" +db_pwd + '@' +db_host + ':' +db_port +'/'+ db_name;

console.log('TODO: db_logger');

function find_one_user(find_criteria, projection) {

    var options = {
        db_url: db_url,
        db_coll: db_colls.userinfo,
        find_criteria:find_criteria,
        projection: { }
    }

   if (projection) options.projection = projection;
    
    return db_methods.find_one(options)
}
//new user
function save_userinfo(userinfo) {
    console.log('save_userinfo');
    
    var options = {
        db_url: db_url,
        db_coll: db_colls.userinfo,
        new_doc: userinfo
    }
    
    return db_methods.insert_one(options)
}

function update_user(find_criteria, update) {
    console.log('update_user');
    var options = {
        db_url: db_url,
        db_coll: db_colls.userinfo,
        update: update,
        find_criteria:find_criteria
    }

    return db_methods.update_one(options)
}

// level up / layer up
function get_basic_user_info(email) {
    var projection = {
        email:1,
        _id:1,
        name:1,
        link:1,
        picture:1,
        gender:1,
        roles:1
    }

    return find_one_user(email, projection)
}

function insert_playlist(user_email, playlist_data) {
    var find_criteria = { email:user_email }
    var update = {
        $push: { 
            yt_playlists : playlist_data
        }
    }

    return update_user(find_criteria, update)
}

function insert_video_to_playlist(opt) {
    
    var find_criteria = { 
        email: opt.user_email, 
        "yt_playlists.id": opt.playlistId 
    };

    var update = {
        $push: { 
            "yt_playlists.$.videos" : opt.video_data
        }
    }

    return update_user( find_criteria, update)
}

function delete_one_user_playlist(user_email, playlist_id) {
    console.log('delete_one_user_playlist');
    
    var find_criteria = {
        email: user_email
    }

    var update = {
        $pull: { 
            yt_playlists: {
                id: playlist_id
            }
        }        
    }

    return update_user(find_criteria, update)
 
}

function delete_user_playlists(user_email, playlists) {
    if (playlists.length > 0) {
        var playlists_to_del = [];
        playlists.forEach(pl => {
            playlists_to_del.push(delete_one_user_playlist(user_email, pl.id));
        })

        return Promise.all(playlists_to_del);

    } else {
        console.log('nothing to del')
        return {deleted:playlists.length}
    }
}

function get_all_user_playlists(user_email) {
    return new Promise( (resolve) => {
        console.log('get_all_user_playlists')
        find_one_user({email: user_email}, { _id: 0, yt_playlists:1} )
        .then( response => resolve(response.yt_playlists))
    })
}

var get_user_YTPlaylists_with_ytid = function(email, ytid) {

    var opt = {
        db_url: db_url,
        db_coll: db_colls.userinfo
    }

    var $match = {
        email: email
    }
    $match["yt_playlists.videos.id"] = ytid,
    
    opt.aggregate_opt = [ { $match: $match },
                       { $unwind:"$yt_playlists"}, 
                       { $unwind:"$yt_playlists.videos" }, 
                       { $match: { "yt_playlists.videos.id":ytid } }, 
                       { $project: { 
                           title: "$yt_playlists.title", 
                           id: "$yt_playlists.id",
                           _id:0 
                        }}]
    
    return db_methods.aggregate(opt)

}

module.exports = {
    find_one_user,
    save_userinfo,
    update_user,
    //level up / layer up:
    get_basic_user_info,
    insert_playlist,
    insert_video_to_playlist,
    get_all_user_playlists,
    delete_one_user_playlist,
    delete_user_playlists,
    get_user_YTPlaylists_with_ytid
}
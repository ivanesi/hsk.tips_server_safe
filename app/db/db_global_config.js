'use strict'
const host = '127.0.0.1',
      port = '27017';

module.exports = {
        dbs: {  
                cnm_eng: {
                        host: host,
                        port: port,
                        name: 'cnm_eng',
                        collections: {
                                wordlistsInfoHSK : 'wordlists.info.hsk',
                                wordsHSK: 'words.hsk'
                        },
                        users: {
                                guest: {
                                        login: 'guest',
                                        pwd: '------'
                                },
                                admin: {
                                        login: 'admin',
                                        pwd: '------',
                                }
                        },
                },
                users: {
                        host: host,
                        port: port,
                        name: 'users',
                        users: {
                                admin: {
                                        name: 'admin',
                                        pwd: '------',
                                }
                        },                        
                        collections: {
                                sessions: 'sessions',
                                userinfo: 'userinfo'
                        }
                }
        }
}
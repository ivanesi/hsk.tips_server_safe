'use strict'
var mongo = require('../db_mongo')

module.exports = function(opt, next) {
    return new Promise( (resolve, reject) => {
        mongo.MongoClient.connect(opt.db_url, function(err, db) {
            if (err) reject(err)
            db.collection(opt.db_coll).insertOne(opt.new_doc, function(err, r) {
                if (err) reject(err)
                db.close();
                resolve(r.result)                
            });
        })   
    })
}
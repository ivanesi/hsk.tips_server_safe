'use strict'
var mongo = require('../db_mongo')

module.exports = function(opt) {

    var limit = 0;
    var skip = 0;    
    return new Promise( (resolve, reject) => {
        mongo.MongoClient.connect(opt.db_url, function(err, db) {
            if (err) reject(err);

            var find_result = [];

            var cursor = db.collection(opt.db_coll)
                .find(opt.find_criteria, opt.projection)
                .skip(skip)
                .limit(limit)
                .count(function(err, count) {
                    if (err) reject(err);
                    db.close();
                    resolve(count);
                });
        })

    })
    
};
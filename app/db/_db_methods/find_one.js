'use strict'
var mongo = require('../db_mongo')

module.exports = function(opt) {
    return new Promise( (resolve, reject) => {
        mongo.MongoClient.connect(opt.db_url, function(err, db) {
            if (err) reject(err);
            db.collection(opt.db_coll).find(opt.find_criteria).limit(1).project(opt.projection).next(function(err, doc) {
                if (err) reject(err);
                db.close();  
                resolve(doc);
            });
        })
    })    
}
'use strict'
var mongo = require('../db_mongo')

module.exports = function (opt) {
    return new Promise( (resolve, reject) => {
        mongo.MongoClient.connect(opt.db_url, function(err, db) {
            if (err) reject(err)
            db.collection(opt.db_coll).updateOne(opt.find_criteria, opt.update, function(err, r) {
                if (err) reject(err)
                db.close();
                if (r) { resolve(r.result) } else resolve(null)     
            });
        })   
    })
}
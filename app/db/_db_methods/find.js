'use strict'
var mongo = require('../db_mongo')

module.exports = function(opt) {
    opt.limit = opt.limit || 0;
    opt.skip = opt.skip || 0;    
    return new Promise (function(resolve, reject) {
        mongo.MongoClient.connect(opt.db_url, function(err, db) {
            if (err) reject(err)
            
            var find_result = [];

            var cursor = db.collection(opt.db_coll)
                    .find(opt.find_criteria,opt.projection)
                    .skip(opt.skip)
                    .limit(opt.limit);

            cursor.each(function(err, doc) {
                if (err) reject(err)
                
                if (doc != null) {
                    find_result.push(doc);
                } else {
                    db.close();
                    resolve(find_result);
                }
            });
        })
    })
};

'use strict'
var mongo = require('../db_mongo')
module.exports = function(opt, next) {
    return new Promise( (resolve, reject) => {
        mongo.MongoClient.connect(opt.db_url, function(err, db) {
            if (err) reject(err)
            db.collection(opt.db_coll).aggregate(opt.aggregate_opt, function(err, docs) {
                if (err) reject(err)
                db.close();
                resolve(docs)                
            });
        })   
    })
}
'use strict'    
module.exports = function(w, ex) {
    var hsk_id = w.hsk_id;
    var ex_id = ex.eid;
    return {
        title : "Chinese HSK "+ w.hsk + " vocabulary " + w.s +" ("+w.p1+"), ex."+ex_id + ", www.hsk.tips",
        description : "Expand your chinese vocabulary with http://hsk.tips\r\n"
                            + "If you are studying chinese, and going to pass HSK, we can help. You can easily create custom youtube playlists from chinese words you want to learn with our tool. Also, on our youtube channel you can find chinese playlists for your HSK level: "
                            + "https://www.youtube.com/channel/UCaUWFF0waodhhDb5zwy8iQQ/playlists\r\n"
                            + " Chinese words were groupped by topics and HSK levels. \r\n\r\n"
                            + "In this video the chinese character is from HSK level "
                            + w.hsk
                            + ". And it is \""
                            + w.s 
                            + "\", pinyin: \""
                            + w.p1 +"\", which means \""
                            + w.means
                            + "\".\r\n"
                            + "Listen to native mandarin speakers saying \""
                            + w.s 
                            + "\" and the example: \""
                            + ex.txt_s
                            + "\", it means \""
                            + ex.tr.txt
                            + "\"\r\n "
                            + "This word is from HSK"
                            + w.hsk
                            + " vocabulary wordlist.",

      tags : [  w.s,
                w.s+" meaning",
                "hsk.tips",
                "http://hsk.tips",
                "hsk tips",
                "hsk", 
                        "chinese",
                        "hsk chinese",
                        "chinese hsk",
                "chinese vocabulary",
                "hsk tips",
                        "hsk"+w.hsk,
                        "hsk "+w.hsk, 
                        "hsk " + w.hsk + " level", 
                        "hsk level " +w.hsk, 
                        "hsk vocabulary",
                        "vocab",
                        "hsk "+w.hsk +" vocabulary", 
                        "hsk" +w.hsk +" vocabulary",
                        "hsk words",
                        "hsk "+ w.hsk+" words",
                        "hsk"+w.hsk+" words",
                        "hsk writing",
                        "hsk "+w.hsk+" writing",
                        "hsk"+w.hsk+" writing",
                        "hsk listening",
                        "hsk"+ w.hsk+" listening",
                            "hsk "+ w.hsk+" listening",
                            "chinese lessons",
                            "chinese lessons mandarin",
                            "learn chinese",
                            "learn mandarin",
                            "hsk preparation",
                            "hsk " +w.hsk + " wordlist",
                            "hsk" +w.hsk + " wordlist"
                    ]
    }
}
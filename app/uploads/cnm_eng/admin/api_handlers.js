'use strict'
const config = require('../../../app.config');
const helpers = require('../../../helpers/files_and_dirs');

const logger = require('../../../logger/logger');

function upload_word_audio(req) {
    
    var what = req.query.what,        
        hsk = req.query.hsk,
        hsk_id = req.query.hsk_id,
        old_filename = req.query.filename_in_db;
    
    helpers.create_dir_if_need_sync(config.cnm_eng_media_dir.audio+'hsk' +hsk);
    helpers.create_dir_if_need_sync(config.cnm_eng_media_dir.audio+'hsk' +hsk+'/' +hsk_id);
    
    var upload_dir = config.cnm_eng_media_dir.audio+'hsk'+hsk+'/'+hsk_id +'/';
   
    if (what === 'hw') {
        var hw_index = Number(req.query.hw_index);
        req.files.hw.new_name = hsk_id+'-hw'+(hw_index+1)+'.'+helpers.get_file_extention(req.files.hw.name);
        return helpers.delete_file(upload_dir+old_filename)
                .then( () => helpers.save_file_to_dir(req.files.hw, upload_dir) )
                .catch( err => logger.error(err) );
    }
    if (what === 'ex') {
        var ex_id = req.query.ex_id;
        req.files.ex.new_name = hsk_id+'-ex'+ex_id+'.'+helpers.get_file_extention(req.files.ex.name);
        return helpers.delete_file(upload_dir+old_filename)
                .then( () => helpers.save_file_to_dir(req.files.ex, upload_dir) )
                .catch( err => logger.error(err) );;
    }
    if (what === 'ex_tr') {
        var ex_id = req.query.ex_id;
        req.files.ex_tr.new_name = hsk_id+'-ex'+ex_id+'-tr.'+helpers.get_file_extention(req.files.ex_tr.name);
        return helpers.delete_file(upload_dir+old_filename)
                .then( () => helpers.save_file_to_dir(req.files.ex_tr, upload_dir) )
                .catch( err => logger.error(err) );;
    }

    
    if (what === 'new_ex') {
        var ex_id = req.query.ex_id;
        req.files.new_ex.new_name = hsk_id+'-ex'+ex_id+'.'+helpers.get_file_extention(req.files.new_ex.name);
        return helpers.save_file_to_dir(req.files.new_ex, upload_dir);
    }
    if (what === 'new_ex_tr')  {
        var ex_id = req.query.ex_id;
        req.files.new_ex_tr.new_name = hsk_id+'-ex'+ex_id+'-tr.'+helpers.get_file_extention(req.files.new_ex_tr.name);
        return helpers.save_file_to_dir(req.files.new_ex_tr, upload_dir);
    }
}

module.exports = {
    upload_word_audio: upload_word_audio
}

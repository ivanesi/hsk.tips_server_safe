'use strict'
var youtube = require('../../../google/youtube/youtube');
var users_handlers = require('../../../users/users_handlers');

var logger = require('../../../logger/logger');

var get_playlists = function(req, res) {
    console.log('get_playlists')
    if (req.session && req.session.tokens) {        
        youtube.playlists.get_playlists(req)
        .then(playlists => res.json( {err:null, playlists:playlists}) )
        .catch(err => logger.error(err) ) 

    } else res.json({err:'need gauth', playlists: null})
}

var get_all_playlistItems = function(req,res) {
    console.log('get_all_playlistItems')
    if (req.session && req.session.tokens) {        
        youtube.playlists.get_all_playlistItems(req)
        .then(playlistItmes => res.json(playlistItmes))
        .catch(err => logger.error(err) ) 

    } else res.json({err:'need gauth'})
}

var insert_playlist = function(req, res) { 
    console.log('insert_playlist')
    if (req.session && req.session.tokens) {
        var response;
        youtube.playlists.insert_playlist(req)
        .then( (resp) => { 
            response = resp; 
            res.json( {err:null, response:response} ) 
        })
        .then( () => users_handlers.insert_yt_playlist(req, response) )
        .catch(err => logger.error(err) ) 

    } else res.json({err:'need gauth', response:null})
}

var insert_playlistItems = function(req, res) {
    console.log('insert_playlistItems');
    if (req.session && req.session.tokens) {
        var response;
        youtube.playlists.insert_playlistItems(req) 
        .then( (resp) => {
            response = resp; 
            res.json( {err:null, response:response}) 
        })
        .then( () => users_handlers.insert_yt_playlistItems(req, response) )
        .catch(err => logger.error(err) ) 
    } else { 
        res.json({err:'need gauth', response:null})
    }
}

module.exports = {
    get_playlists,
    get_all_playlistItems,
    insert_playlist,
    insert_playlistItems,
}
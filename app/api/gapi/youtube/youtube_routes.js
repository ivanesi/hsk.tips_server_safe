'use strict'
const api_path = require('../../api_paths');
const gapi_paths = api_path.gapi;

const youtube_handlers = require('./youtube_handlers');

module.exports = function(server) {
    server.get(gapi_paths.youtube.playlists, youtube_handlers.get_playlists); 
    server.get(gapi_paths.youtube.get_all_playlistItems, youtube_handlers.get_all_playlistItems); 
    server.post(gapi_paths.youtube.insert_playlist, youtube_handlers.insert_playlist);
    server.put(gapi_paths.youtube.insert_playlistItems, youtube_handlers.insert_playlistItems);
}
'use strict'
var youtube_routes = require('./youtube_routes');

module.exports = function(server) {
    youtube_routes(server);
};
'use strict'
var oauth2 = require('./oauth2/oauth2');
var youtube =require('./youtube/youtube');

module.exports = function(server) {
    oauth2(server);
    youtube(server)
};
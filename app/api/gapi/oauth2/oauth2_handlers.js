'use strict'
var oauth2 = require('../../../google/oauth2/oauth2');
var users_handlers = require('../../../users/users_handlers');

var logger = require('../../../logger/logger');

var get_oauth2_url = function(req, res) {
    var oauth2_client = oauth2.get_oauth2_client()
    var url = oauth2.generate_oauth2_url(oauth2_client)
    res.json( { oauth2_url: url } )
}

var oauth2_handler = function(req, res) {
    console.log('oauth2_handler')
    let google_code, oauth2_client, tokens;

    oauth2.check_google_code(req)
    .then( code => { 
        google_code = code; 
        oauth2_client = oauth2.get_oauth2_client();
    })
    .then( () => oauth2.get_access_tokens(oauth2_client, google_code))
    .then( t => { 
        tokens = t; 
        oauth2_client.setCredentials(tokens);
    })
    .then( () => oauth2.save_tokens_in_session(req, tokens) )
    .then( () => oauth2.get_user_data(oauth2_client) )
    .then( (userinfo) => users_handlers.get_google_user_data(req, userinfo) )
    .then( () => console.log('>> oauth2_handler: redirect') )
    .then( () => res.redirect(req.query.state) )    
    .catch( err => logger.error(err) );
}

module.exports = {
    get_oauth2_url: get_oauth2_url,
    oauth2_handler: oauth2_handler,
}
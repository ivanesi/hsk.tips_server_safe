'use strict'
const api_path = require('../../api_paths');
const gapi_paths = api_path.gapi;

const oauth2_handlers = require('./oauth2_handlers');

module.exports = function(server) {
    // oauth2        
    server.get(gapi_paths.oauth2.get_oauth2_url, oauth2_handlers.get_oauth2_url);  //can only one time in app start  
    server.get(gapi_paths.oauth2.oauth2_handler, oauth2_handlers.oauth2_handler);
}
'use strict'
var oauth2_routes = require('./oauth2_routes');

module.exports = function(server) {
    oauth2_routes(server);
};
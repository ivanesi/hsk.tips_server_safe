'use strict'

const api_cnm_eng = require('./cnm_eng/api_cnm_eng');
const api_gapi = require('./gapi/gapi');
const api_user = require('./user/api_user');

module.exports = {
    start: function(server) {
        api_cnm_eng(server);
        api_gapi(server);
        api_user(server);
    }
}

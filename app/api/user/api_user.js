'use strict'
const api_paths = require('../api_paths');
const api_user_handlers = require('./api_user_handlers');

module.exports = function(server) {
    // oauth2        
    server.get(api_paths.user.logout, api_user_handlers.logout); 
    server.get(api_paths.user.get_userinfo, api_user_handlers.get_userinfo);
    server.get(api_paths.user.sync_google_data, api_user_handlers.sync_google_data);
}
'use strict'
var db_handlers = require('../../db/users/db_handlers');
var youtube = require('../../google/youtube/youtube');
var users_handlers = require('../../users/users_handlers');

var logger = require('../../logger/logger');

function logout(req, res) {
   req.session.destroy(function(err) {
       if (err) logger.error(err)
       if (req.query.google == 'true') {
            res.redirect('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue='+req.query.redirectUrl);
       } else {
            res.redirect(req.query.redirectUrl)
       }
   })
}

function get_userinfo(req, res) {
    console.log('api_user_handlers, get_userinfo')   
    if (req.session && req.session.tokens && req.session.tokens.refresh_token) {
        db_handlers.get_basic_user_info({email:req.session.user_email})
        .then(userinfo => res.json( { err: null, userinfo: userinfo } ))
        .catch(err => logger.error(err) )

    } else {
        res.json( { err: "no google credentials", userinfo: null } )
        console.log('get_user_data: no google credentials')
    }
}

function sync_google_data(req, res) {
    console.log('api_user_handlers, sync_google_data');

    users_handlers.sync_data.sync_yt_playlists(req)
    .then( (r) => res.json(r) )
    .catch(err => logger.error(err) )
}

module.exports = {
    logout:logout,
    get_userinfo: get_userinfo,
    sync_google_data:sync_google_data
}
'use strict'
const logger = require('../../../logger/logger');

const api_paths = require('../../api_paths');
const config = require('../../../app.config')

const db_admin_handlers = require('../../../db/cnm_eng/admin/db_admin_handlers');

const upload = require('../../../uploads/cnm_eng/admin/api_handlers');

const ffmpeg_get_duration = require('../../../ffmpeg/audio/get_duration');
const ffmpeg_normalize = require('../../../ffmpeg/audio/normalize');
const ffmpeg_slowdown = require('../../../ffmpeg/audio/slowdown');

const create_images_and_video = require('../../../create_media/cnm_eng/create_images_and_video');
const create_full_audio = require('../../../create_media/cnm_eng/create_audio/create_full_audio_for_example');

const set_yt_video_descriprion = require('../../../uploads/cnm_eng/admin/set_youtube_video_description');

const youtube = require('../../../google/youtube/youtube');

const db_users_handlers = require('../../../db/users/db_handlers')

var get_hsk_wordlists_basic_info = function(req, res) {
      console.log('get_hsk_wordlists_basic_info')
      if (req.session.is_admin) {            
            db_admin_handlers.get_hsk_wordlists_basic_info()
            .then( result => res.json(result) )
            .catch( err => logger.error(err) )
      } else {
            res.send({err: 'who are you?'})
      }
};

var get_hsk_words = function(req, res) {
      console.log('get_hsk_words')      
      if (req.session.is_admin) {
            db_admin_handlers.get_hsk_words(req)
            .then( result => res.json(result))
            .catch( err => logger.error(err) )
      } else {
            res.send({err: 'who are you?'})
      }
}

var edit_word = function(req, res) {
      console.log('edit_word')
      if (req.session.is_admin) {        
            var _id = req.query._id;      
            res.redirect(api_paths.cnm_eng.admin.edit_word+'/'+_id);
      } else {
            res.send({err: 'who are you?'})
      }
}

var get_full_word = function(req, res) {
      console.log('get_full_word')
      if (req.session.is_admin) {
            var _id = req.query._id
            db_admin_handlers.get_one_word_by_id(_id)
                  .then(word => res.json(word))
                  .catch( err => logger.error(err))
      } else {
            res.send({err: 'who are you?'})
      }
}

var upload_word_audio = function(req, res) {
      console.log('upload_word_audio');
      if (req.session.is_admin) {
            req.query._id = req.query.w_id;      
            var files = {};

            upload.upload_word_audio(req)
                  .then( file => files.norm = { f: file} )
                  .then( file => ffmpeg_normalize(files.norm.f) )
                  .then( file => {
                        if ( (req.query.what !== "ex_tr") && (req.query.what !== "new_ex_tr") ) { 
                              return ffmpeg_slowdown(files.norm.f) 
                        } else return null
                  })
                  .then( file => files.slow = {f:file} )
                  .then( () => ffmpeg_get_duration(files.norm.f) )
                  .then( dur => files.norm.dur = dur )
                  .then( () => {
                        if (files.slow.f) {
                              return ffmpeg_get_duration(files.slow.f) 
                        } else return null
                  })
                  .then( dur => files.slow.dur = dur)
                  .then( () => db_admin_handlers.save_media_to_db(req, files) )
                  // .then( res => console.log(res))
                  .then( () => edit_word(req, res) )
                  .catch( (err) => logger.error(err) )

      } else {
            res.send({err: 'who are you?'})
      }
}

var update_whole_word = function(req, res) {
      console.log('update_whole_word')
      if (req.session.is_admin) {
            var word = req.body;
            db_admin_handlers.update_one_word({_id:word._id}, word)
                  .then( response => res.json(response) )
                  .catch(err => logger.error(err) )
      } else {
            res.send({err: 'who are you?'})
      }
}

var create_video = function(req, res) {
      console.log('create_video')
      if (req.session.is_admin) {
            var word = req.body.word;
            var ex = req.body.ex;
            create_images_and_video(word, ex)
                  .then(result => res.json(result) )
                  .catch(err => logger.error(err) )
      } else {
            res.send({err: 'who are you?'})
      }
}

var create_full_audio_of_example = function(req,res) {
      console.log('create_full_audio_of_example')
      if (req.session.is_admin) {
            var word = req.body.word;
            var ex = req.body.ex;
            create_full_audio(word, ex)
                  .then(result => res.json(result) )
                  .catch(err => logger.error(err) )
      } else {
            res.send({err: 'who are you?'})
      }
}

var upload_video_to_youtube = function(req, res) {
      console.log('upload_video_to_youtube')
      if (req.session.is_admin) {          
            var w = req.body.word,
            ex = req.body.ex;
            var desc = set_yt_video_descriprion(w, ex);
            var options = {
                  filename: config.cnm_eng_media_dir.video+'hsk'+w.hsk+'/'+w.hsk_id+'EX'+ex.eid+'.mp4',
                  title: desc.title,
                  description: desc.description,
                  tags: desc.tags
            }      
            if (req.session && req.session.tokens) {
                  var uploadProcess = youtube.videos.insert_video(req, options) 
                  .then( response => db_admin_handlers.save_ytid_to_db(w, ex, response))
                  .then( response => res.json({err:null, response:response}) )
                  .catch(err => logger.error(err) ) 
            } else { 
                  res.json({err:'need gauth', response:null})
            }
      
      } else {
            res.send({err: 'who are you?'})
      }

}

var get_user_YTPlaylists_with_ytid = function(req, res) {
      if (req.session.is_admin) {
            var ytid = req.query.ytid;
            var email = req.session.user_email;
            db_users_handlers.get_user_YTPlaylists_with_ytid(email, ytid)
                  .then( result => res.json(result) )
      } else {
            res.send({err: 'who are you?'})
      }
}

var get_ex_toDel = function(req, res) {
      if (req.session.is_admin) {
            db_admin_handlers.get_ex_toDel()
                  .then( result => res.json(result) )
                  .catch( err => logger.error(err) )
      } else {
            res.send({err: 'who are you?'})
      }
}

module.exports = {
      get_hsk_wordlists_basic_info,
      get_hsk_words,
      edit_word,
      get_full_word,
      upload_word_audio,
      update_whole_word,
      create_video,
      create_full_audio_of_example,
      upload_video_to_youtube,
      get_user_YTPlaylists_with_ytid,
      get_ex_toDel
}


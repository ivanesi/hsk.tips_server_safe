'use strict'

var api_paths = require('../../api_paths.js');
var api_handlers_admin = require('./api_admin_handlers');

module.exports = function(server) {
    //
    server.get(api_paths.cnm_eng.admin.get_hsk_wordlists_basic_info, api_handlers_admin.get_hsk_wordlists_basic_info);
    server.get(api_paths.cnm_eng.admin.get_hsk_words, api_handlers_admin.get_hsk_words);
    server.get(api_paths.cnm_eng.admin.edit_word, api_handlers_admin.edit_word);
    server.get(api_paths.cnm_eng.admin.get_full_word, api_handlers_admin.get_full_word);
    server.get(api_paths.cnm_eng.admin.get_user_YTPlaylists_with_ytid, api_handlers_admin.get_user_YTPlaylists_with_ytid);
    server.get(api_paths.cnm_eng.admin.get_ex_toDel, api_handlers_admin.get_ex_toDel);
    
    //create video
    server.post(api_paths.cnm_eng.admin.create_video_for_example, api_handlers_admin.create_video);
    server.post(api_paths.cnm_eng.admin.create_full_audio_of_example, api_handlers_admin.create_full_audio_of_example);
    server.post(api_paths.cnm_eng.admin.upload_word_audio, api_handlers_admin.upload_word_audio);
    server.post(api_paths.cnm_eng.admin.update_whole_word, api_handlers_admin.update_whole_word);
    server.post(api_paths.cnm_eng.admin.upload_video_to_youtube, api_handlers_admin.upload_video_to_youtube);
    
}


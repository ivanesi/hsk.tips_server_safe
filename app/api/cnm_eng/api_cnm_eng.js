'use strict'

var api_guest_routes = require('./api_guest_routes');
var api_admin_routes = require('./admin/api_admin_routes')

module.exports = function(server) {
    api_guest_routes(server);
    api_admin_routes(server);
};
'use strict'
var logger = require('../../logger/logger');

var db_guest_handlers = require('../../db/cnm_eng/guest/db_guest_handlers');

var get_hsk_wordlists_basic_info = function(req, res) {
      console.log('get_hsk_wordlists_basic_info')
      db_guest_handlers.get_hsk_wordlists_basic_info()
      .then( result => res.json(result) )
      .catch( err => logger.error(err) )
};

var get_hsk_words = function(req, res) {
      db_guest_handlers.get_hsk_words(req)
      .then( result => res.json(result))
      .catch( err => logger.error(err) )
}

module.exports = {
      get_hsk_wordlists_basic_info: get_hsk_wordlists_basic_info,
      get_hsk_words: get_hsk_words
}


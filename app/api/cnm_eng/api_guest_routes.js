'use strict'

var api_paths = require('../api_paths.js');
var api_handlers_guest = require('./api_guest_handlers');

module.exports = function(server) {
    //
    server.get(api_paths.cnm_eng.guest.get_hsk_wordlists_basic_info, api_handlers_guest.get_hsk_wordlists_basic_info);
    server.get(api_paths.cnm_eng.guest.get_hsk_words, api_handlers_guest.get_hsk_words);
    
}


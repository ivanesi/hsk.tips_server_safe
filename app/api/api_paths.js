module.exports = {
    root: '/api',
    gapi: {
        root: '/api/service/gapi',
        oauth2: { 
            root: '/api/gapi/oauth',
            get_oauth2_url: '/api/gapi/oauth2/get_url',
            oauth2_handler: '/api/gapi/oauth2/oauth2_handler',
        },
        youtube: {
            playlists: '/api/gapi/youtube/playlists',
            get_all_playlistItems: '/api/gapi/youtube/playlists/get_all_playlistItems',
            insert_playlist: '/api/gapi/youtube/playlists/insert',
            insert_playlistItems: '/api/gapi/youtube/playlistsItems/insert',
        }
    },
    user: {
        get_userinfo: '/api/user/userinfo',
        sync_google_data: '/api/user/sync_g',
        logout: '/api/user/logout'
    },
    cnm_eng: {
        root: '/api/cnm_eng',
        guest: {
            root: '/api/cnm_eng/guest',
            get_hsk_wordlists_basic_info: '/api/cnm_eng/guest/get_hsk_wordlists_basic_info',
            get_hsk_words: '/api/cnm_eng/guest/get_hsk_words'
        },
        admin: {
            adminka: '/adminka',
            edit_word: '/adminka/edit_word',
            
            create_video_for_example: '/api/cnm_eng/admin/create_video_for_example',
            create_full_audio_of_example: '/api/cnm_eng/admin/create_full_audio_of_example',

            update_whole_word: '/api/cnm_eng/admin/update_whole_word',
            
            get_hsk_wordlists_basic_info: '/api/cnm_eng/admin/get_hsk_wordlists_basic_info',
            get_hsk_words: '/api/cnm_eng/admin/get_hsk_words',
            get_full_word :'/api/cnm_eng/admin/get_full_word',
            
            upload_word_audio: '/api/cnm_eng/admin/upload_word_audio',

            upload_video_to_youtube: '/api/cnm_eng/admin/upload_video_to_youtube',

            get_user_YTPlaylists_with_ytid: '/api/cnm_eng/admin/get_user_YTPlaylists_with_ytid',

            get_ex_toDel: '/api/cnm_eng/admin/get_ex_toDel',

        }

    }    
}
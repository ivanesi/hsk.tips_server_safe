'use strict'
const bodyParser = require('body-parser');

const config = require('./app.config');

var logger_errors = require('./logger/logger_errors');
var logger_http = require('./logger/logger_http');

var api = require('./api/api.js');

var session = require('./users/session');

var fileUpload = require('express-fileupload');

module.exports = {
    start : function(express, server) {
        
        server.use(bodyParser.json());
        server.use(session);
                
        server.use(fileUpload());

        server.use(logger_http);
        server.use(logger_errors);

        api.start(server);        

    }
};


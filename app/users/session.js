'use strict'
var logger = require('../logger/logger');

var session = require('express-session')

var db_config = require('../db/db_global_config');

var MongoDBStore = require('connect-mongodb-session')(session);

var db_user = db_config.dbs.users.users.admin.name,
    db_pwd = db_config.dbs.users.users.admin.pwd,
    db_host = db_config.dbs.users.host,
    db_port = db_config.dbs.users.port,
    db_name = db_config.dbs.users.name,
    db_colls = db_config.dbs.users.collections;

var store = new MongoDBStore({ 
    uri: 'mongodb://' +db_user + ":" +db_pwd + '@' +db_host + ':' +db_port +'/'+ db_name,                    
    collection: db_colls.sessions
})

// Catch errors 
store.on('error', function(error) {
    logger.error(error)
});

module.exports = session({
    secret: 'asdfasas1e09vmsl0]dfw23j;239n u[masdjf0]',
    cookie: {
        //secure: true,
        maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week 
    },
    resave: true,
    saveUninitialized: true,
    store: store
})
'use strict'
var db_handlers = require('./../../db/users/db_handlers');
var youtube = require('../../google/youtube/youtube');

var logger = require('../../logger/logger');

function sync_yt_playlists(req) {
    console.log('sync_yt_playlists');
    var yt_playlists;
    var user_email = req.session.user_email;

    return youtube.playlists.get_all_playlists(req)
        .then( yt_plsts => yt_playlists = yt_plsts )
        .then( () => db_handlers.get_all_user_playlists(user_email) )
        .then( user_playlists => _compare_playlists(user_playlists, yt_playlists) )
        .then( playlists_to_del => db_handlers.delete_user_playlists(user_email, playlists_to_del) )
        .catch( err => logger.error(err) )


    function _compare_playlists(user_playlists, yt_playlists) {
        
        var user_playlists_to_delete = [];
        
        user_playlists_to_delete = user_playlists.filter( u_pl => {
            var is_in = false;
            yt_playlists.forEach(yt_pl => {
                if (u_pl.id == yt_pl.id) is_in = true;
            })
            return !is_in
        })

        return user_playlists_to_delete
    }
    

}

module.exports = {
    sync_yt_playlists
}
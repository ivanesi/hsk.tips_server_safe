'use strict'

const db_handlers = require('../../db/users/db_handlers');

module.exports = function(req, response) {
    
    console.log('handle_insert_yt_playlist');
    
    var playlist_data = {
                id: response.id,
                title: response.snippet.title,
                videos: []
    }
    
    return db_handlers.insert_playlist(req.session.user_email, playlist_data)
}
 
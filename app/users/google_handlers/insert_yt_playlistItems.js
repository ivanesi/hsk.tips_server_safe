'use strict'

const db_handlers = require('../../db/users/db_handlers');

module.exports = function(req, response) {
    
    console.log('handle_insert_yt_playlistItems');

    var opt = {
            user_email: req.session.user_email,
            playlistId: response.snippet.playlistId,
            video_data : {
                title: response.snippet.title,
                id: response.snippet.resourceId.videoId
            }
    }    
   
    return db_handlers.insert_video_to_playlist(opt);
}

'use strict'
const db_handlers = require('./../../db/users/db_handlers');
const logger = require('../../logger/logger');

const sync_google_data = require('./sync_data');

var if_user_admin = require('../admin/if_user_admin');

module.exports = function(req, userinfo) {
    console.log('handle_get_google_user_data');
 
    req.session.user_email = userinfo.email;
    req.session.save();
    
    if_user_admin(req);

    return db_handlers.find_one_user({email:userinfo.email}, {email:1, gauth_refresh_token:1} )
        .then(user => {
            if (user) {               
               return _handle_old_user(req, user)
            } else {
               return _handle_new_user(req, userinfo)
            }   
        }).catch(err => logger.error(err) )

}

function _handle_old_user(req, user) {
    console.log('old user');
    var update = { 
        $set: {
            last_time: req.session.gauth_date 
        }
    };

    if (req.session.tokens.refresh_token) {
        console.log('> but new refresh_token')
        update.$set.gauth_refresh_token = req.session.tokens.refresh_token
    } else {
        console.log('> save refresh_token to session');      
        req.session.tokens.refresh_token = user.gauth_refresh_token;
        req.session.save();        
    }
    
    return db_handlers.update_user( {email:user.email},update)
                .then( () => sync_google_data.sync_yt_playlists(req))
                .catch( err => logger.error(err) )
                
}

function _handle_new_user(req, userinfo) {
    console.log('new user');
    
//create user role
    userinfo.roles = [];
    
    if (req.session.is_admin) {
        userinfo.roles.push('sadmin');
    }
//save refresh_token    
    if (req.session.tokens.refresh_token) {
        userinfo.gauth_refresh_token = req.session.tokens.refresh_token;
    }

    var google_id = userinfo.id;
    userinfo.google_id = google_id;
    delete userinfo.id;

    userinfo.yt_playlists = [];
    
    userinfo.first_time = req.session.gauth_date;
    return db_handlers.save_userinfo(userinfo)
}


'use strict'
var admins_array = require('./admins_array');

module.exports = function(req) {
      console.log('check if user is admin')
      console.log('TODO: save it in session and use session in checking ')
      var is_admin = false
      admins_array.forEach(admin => {
            if (req.session.user_email == admin) {
                  is_admin = true
                  req.session.is_admin = true;
                  req.session.save();  
            }
      })      
      return is_admin
}